#!/usr/bin/python

from Tkinter import BOTH, Menu, Frame, Listbox, Scrollbar, END, RIGHT, Y, LEFT
from ttk import Style
import logging

from snap import SnapCom
from csv_rows import CSV

log = logging.getLogger('main')


class App(Frame):
    def __init__(self, parent, conf, sql):
        Frame.__init__(self, parent)
        self.scrollbar = Scrollbar(self)
        self.scrollbar.pack(side=RIGHT, fill=Y)
        self.irrigation_list = Listbox(self, yscrollcommand=self.scrollbar.set)
        self.parent = parent
        self.conf = conf
        self.sql = sql
        self.snap_com = SnapCom(conf)
        self.poll = False
        self.initUI()
        self.test = None

    def initUI(self):
        self.parent.title("Irrigation Setup")
        self.style = Style()
        self.style.theme_use("default")

        self.pack(fill=BOTH, expand=1)

        menubar = Menu(self.parent)

        fileMenu = Menu(menubar)
        fileMenu.add_command(label="Load New CSV", command=self.load_data)
        fileMenu.add_command(label="Start", command=self.start_loop)
        fileMenu.add_command(label="Stop", command=self.stop_loop)
        fileMenu.add_command(label="Exit", command=self.on_exit)
        menubar.add_cascade(label="File", menu=fileMenu)

        testMenu = Menu(menubar)
        testMenu.add_command(label="Start Test", command=self.test_all)
        testMenu.add_command(label="Stop Test", command=self.test_stop)
        menubar.add_cascade(label="Test", menu=testMenu)

        self.parent.config(menu=menubar)

        self.poll = True

        self.after(100, self.snap_internal)

        self.loop()

        self.irrigation_box()

    def test_all(self):
        self.snap_com.test_relay()
        self.test = self.after(10000, self.test_all)

    def test_stop(self):
        self.after_cancel(self.test)

    def load_data(self):
        CSV(self.conf, self.sql)
        self.irrigation_box()

    def on_exit(self):
        self.snap_com.stop()
        self.quit()

    def start_loop(self):
        self.poll = True
        self.loop()

    def stop_loop(self):
        self.poll = False
        self.loop()

    def loop(self):
        if self.poll:
            self.after(1000, self.after_snap_loop_cb)

    def after_snap_loop_cb(self):
        if self.poll:
            self.snap_com.check_schedule_loop(self.sql)
            self.after(5000, self.after_snap_loop_cb)

    def snap_internal(self):
        self.snap_com.run_internals()
        self.after(self.snap_com.snap_interval, self.snap_internal)

    def irrigation_box(self):
        self.irrigation_list.delete(0, END)
        irrigation = self.sql.execute('select time, func, sol, day from irrigation')
        for rows in enumerate(irrigation):
            if rows[1]:
                for sample in rows[1]:
                    text = ''.join([sample[0], ', ', sample[1], ', ', str(sample[2])])
                    self.irrigation_list.insert(END, text)

        self.irrigation_list.pack(side=LEFT, fill=BOTH)
        self.scrollbar.config(command=self.irrigation_list.yview)
