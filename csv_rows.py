import csv


class CSV:
    def __init__(self, conf, sql):
        sql.execute('delete from irrigation')
        with open(conf.csv_file, 'r') as csv_file:
            self.csv = csv.reader(csv_file)
            for row in self.csv:
                dic = {'time': row[0], 'func': row[1], 'sol': row[2]}
                if not dic['sol']:
                    dic['sol'] = ""
                else:
                    dic['sol'] = dic['sol']
                # TODO: Some validation here
                query = 'insert into irrigation (time, func, sol) ' \
                        'values(' + \
                        '"' + str(dic['time']) + '", ' + \
                        '"' + str(dic['func']) + '", ' + \
                        '"' + str(dic['sol']) + '")'
                sql.execute(query)
