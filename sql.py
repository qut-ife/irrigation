import sqlite3
import logging

log = logging.getLogger('main')


class SQL:
    def __init__(self, db_path):
        self.db_path = db_path

    def execute(self, query):
        connection = sqlite3.connect(self.db_path)
        cursorobj = connection.cursor()
        try:
            cursorobj.execute(query)
            result = cursorobj.fetchall()
            connection.commit()
            id = cursorobj.lastrowid
        except sqlite3.IntegrityError as er:
            log.error('Integrity Error: ' + er.message + ', ' + query)
            raise
        except sqlite3.DatabaseError as er:
            log.error('Database Error: ' + er.message + ', ' + query)
            raise
        except sqlite3.Error as er:
            log.error('SQL Error: ' + er.message + ', ' + query)
            raise
        connection.close()
        return result, id
