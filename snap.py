from snapconnect import snap
from apy import ioloop_scheduler
from sys import platform as _platform

import logging
import os
import time
import binascii

log = logging.getLogger('main')


class SnapCom(object):
    def __init__(self, conf):
        snapAddr = None  # Intrinsic address for Bridge
        self.bridge_address = None
        self.snap_interval = 5  # ms
        self.conf = conf
        self.board = conf.board
        self.snapRpcFuncs = {
            "self_callback": self.self_callback
        }

        # Create SNAP Connect instance. Note: we are using TornadoWeb's scheduler.
        self.snapconnect = snap.Snap(license_file=os.path.join(conf.appPath, 'license.dat'),
                                     addr=snapAddr,
                                     scheduler=ioloop_scheduler.IOLoopScheduler.instance(),
                                     funcs=self.snapRpcFuncs
                                     )

        self.open()

        # No encryption
        self.snapconnect.save_nv_param(snap.NV_AES128_ENABLE_ID, False)

        # Lock down our routes (we are a stationary device)
        self.snapconnect.save_nv_param(snap.NV_MESH_ROUTE_AGE_MAX_TIMEOUT_ID, 0)

        # Don't allow others to change our NV Parameters
        self.snapconnect.save_nv_param(snap.NV_LOCKDOWN_FLAGS_ID, 0x2)

        # tornado.ioloop.PeriodicCallback(self.snapconnect.poll_internals, self.snap_interval).start()

        # self.checkSampleScheduleLoopPeriodic = tornado.ioloop.PeriodicCallback(self.check_schedule_loop, 5000)
        # self.checkSampleScheduleLoopPeriodic.start()

        self.snapconnect.set_hook(snap.hooks.HOOK_SERIAL_OPEN, self.hook_open)
        self.snapconnect.set_hook(snap.hooks.HOOK_SERIAL_CLOSE, self.hook_close)
        self.snapconnect.set_hook(snap.hooks.HOOK_RPC_SENT, self.hook_rpc_sent)

        snapconnect_addr = self.snapconnect.local_addr()
        log.info("SNAPconnect Address: %r" % binascii.hexlify(snapconnect_addr))

        self.snapconnect.NV_MESH_RREQ_TRIES_ID = 25
        self.snapconnect.NV_MESH_RREQ_WAIT_TIME_ID = 26
        self.snapconnect.NV_MESH_INITIAL_HOPLIMIT_ID = 27

        self.relay = 0

    def open(self):
        if _platform == "linux" or _platform == "linux2":
            self.snapconnect.open_serial(1, self.conf.snapSerial, reconnect=True, )
        elif _platform == "win32":
            self.snapconnect.open_serial(self.conf.snapType, self.conf.snapSerial, reconnect=True, )
        else:
            self.snapconnect.open_serial(1, self.conf.snapSerial, reconnect=True, )

    def run_internals(self):
        self.snapconnect.poll_internals()

    def check_schedule_loop(self, sql):
        # log.debug("check_schedule_loop: " + time.strftime("%H:%M:%S"))
        irrigation = sql.execute('select time, func, sol, day from irrigation')
        for rows in enumerate(irrigation):
            if rows[1]:
                for sample in rows[1]:
                    current_time = time.strftime("%H:%M")
                    current_date = time.strftime("%d-%m-%Y")
                    is_day = sample[3] != current_date
                    is_not_done = sample[3] is not current_date
                    if sample[0] == current_time and is_day and is_not_done:
                        log.debug(sample)
                        sql.execute(
                            "update irrigation set day='" + current_date
                            + "' where time = '" + current_time + "'"
                        )
                        log.info(str(sample[0]) + " " + str(sample[1]) + " " +
                                 str(sample[2]) + " " + str(sample[3]))
                        self.sendCommand(sample[1], sample[2])
                        break

    def sendCommand(self, cmd, args):
        log.debug("=======Send CMD=========")
        self.open()
        if not args:
            self.snapconnect.rpc(self.board.decode('hex'), 'callback', 'self_callback', str(cmd))
            # self.snapconnect.mcast_rpc(1, 3, 'callback', 'self_callback', str(cmd))
        else:
            self.snapconnect.rpc(self.board.decode('hex'), 'callback', 'self_callback', str(cmd), int(args))
            # self.snapconnect.mcast_rpc(1, 3, 'callback', 'self_callback', str(cmd), int(args))

        log.debug("CMD: " + str(cmd) + " " + str(args))

    def self_callback(self, message):
        log.debug("=======callback=========")
        log.info(message)

    def hook_open(self, serial_type, port, addr=None):
        """Callback function invoked when a serial connection is opened."""
        if addr:
            self.bridge_address = addr
            log.info("Serial connection opened to %s", binascii.hexlify(addr))
        else:
            log.critical("Unable to open serial connection")

        return addr

    def hook_close(self, serial_type, port):
        """Callback function invoked when a serial connection is closed."""
        log.info("Serial connection closed")

    def hook_rpc_sent(self, packet_identifier, transmit_success):
        """Callback function invoked when a packet is sent."""
        log.debug("packet_identifier: " + str(packet_identifier) + ", transmit_success: " + str(transmit_success))

    def test_relay(self):
        log.info("=======Test Relay=========")
        self.open()
        self.relay += 1
        if self.relay > 5:
            self.relay = 1
        self.snapconnect.rpc(self.board.decode('hex'), 'callback', 'self_callback', 'relay_on', self.relay)

    def stop(self):
        """Stop the SNAPconnect instance."""
        self.snapconnect.close_all_serial()  # Close all serial connections opened with SNAPconnect
