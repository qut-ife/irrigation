#!/usr/bin/python

from Tkinter import Tk
import logging
import tornado.escape
import tornado.ioloop
import tornado.options

from snap import SnapCom
from conf import Configuration
from sql import SQL
from app import App

log = logging.getLogger('main')
conf = Configuration()

sql = SQL(conf.db)


def main():
    if conf.logLevel == logging.DEBUG:
        logging.basicConfig(level=conf.logLevel, format='%(asctime)-15s %(levelname)-8s %(name)-8s %(message)s')
    logging.basicConfig(filename=conf.logFile, format='%(asctime)-15s %(levelname)-8s %(name)-8s %(message)s',
                        filemode='a', level=conf.logLevel)
    log.info("***** Begin Console Log *****")
    log.info("logging level: " + str(conf.logLevel))

    tornado.options.parse_command_line()
    tornado.options.logging = conf.logLevel
    log.setLevel(conf.logLevel)
    logging.getLogger("tornado.access").setLevel(conf.logLevel)

    snaplib_log = logging.getLogger('snap')
    snaplib_log.setLevel(conf.logLevel)

    root = Tk()
    root.geometry("300x400")
    App(root, conf, sql)
    root.mainloop()


if __name__ == '__main__':
    main()
