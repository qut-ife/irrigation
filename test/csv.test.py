import os
import csv

csvFile = os.path.join('..', 'resources', 'irrigation_automation_test.csv')


class CSV:
    csv_rows = []

    def __init__(self, file):
        with open(file, 'r') as csv_file:
            self.csv = csv.reader(csv_file)
            for row in self.csv:
                dic = {'t': row[0], 'f': row[1], 's': row[2]}
                self.csv_rows.append(dic)


csvC = CSV(csvFile)

for sample in csvC.csv_rows:
    print sample['t']
