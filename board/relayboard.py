debug = 0
rel = (0, 21, 20, 19, 9, 15)  # relay pins skipping 0 pos
bkt_pin = (0, 23, 31, 30, 29, 28)  # tipping bucket pins

rate1 = 10
rate2 = 10
rate3 = 10
rate4 = 10
rate5 = 10

bkt1 = 0
bkt2 = 0
bkt3 = 0
bkt4 = 0
bkt5 = 0

cur_ml1 = 0
cur_ml2 = 0
cur_ml3 = 0
cur_ml4 = 0
cur_ml5 = 0

chun_ml1 = 0
chun_ml2 = 0
chun_ml3 = 0
chun_ml4 = 0
chun_ml5 = 0

timer1 = 0
timer2 = 0
timer3 = 0
timer4 = 0
timer5 = 0

int_rate = 10
int_timer = 60

watering = 0
Plot_done = 0b100000
wait_flag = 0b000000


@setHook(HOOK_STARTUP)
def _serf_start():
    global rate1, rate2, rate3, rate4, rate5, int_timer, int_rate
    _set_write(rel[1], 1, 0)
    _set_write(rel[2], 1, 0)
    _set_write(rel[3], 1, 0)
    _set_write(rel[4], 1, 0)
    _set_write(rel[5], 1, 0)
    _set_write(bkt_pin[1], 0, 0)
    _set_write(bkt_pin[2], 0, 0)
    _set_write(bkt_pin[3], 0, 0)
    _set_write(bkt_pin[4], 0, 0)
    _set_write(bkt_pin[5], 0, 0)
    rate1 = loadNvParam(128)
    rate2 = loadNvParam(129)
    rate3 = loadNvParam(130)
    rate4 = loadNvParam(131)
    rate5 = loadNvParam(132)
    int_rate = loadNvParam(133)
    int_timer = loadNvParam(134)


@setHook(HOOK_100MS)
def _MS_hook():
    _watering_block()
    if debug: a = str(watering) + "," + str(_testBit(Plot_done, watering - 1)) + "," + str(
        _testBit(wait_flag, watering - 1))
    if debug: print a


@setHook(HOOK_1S)
def _S_hook():
    _timers()


@setHook(HOOK_GPIN)
def _pin_trigger(pin, isSet):
    _rain_counter(pin, isSet)


def _set_write(pin, dir, set):
    setPinDir(pin, dir)
    if dir:
        writePin(pin, set)
    else:
        monitorPin(pin, 1)


def relay_on(relay):
    relay_off()
    writePin(rel[relay], 1)
    return relay


def relay_off():
    writePin(rel[1], 0)
    writePin(rel[2], 0)
    writePin(rel[3], 0)
    writePin(rel[4], 0)
    writePin(rel[5], 0)
    return 'off'


def _timers():
    global timer1, timer2, timer3, timer4, timer5, wait_flag
    if timer1 > 1:
        timer1 -= 1
    else:
        wait_flag = _clearBit(wait_flag, 0)
    if timer2:
        timer2 -= 1
    else:
        wait_flag = _clearBit(wait_flag, 1)
    if timer3:
        timer3 -= 1
    else:
        wait_flag = _clearBit(wait_flag, 2)
    if timer4:
        timer4 -= 1
    else:
        wait_flag = _clearBit(wait_flag, 3)
    if timer5:
        timer5 -= 1
    else:
        wait_flag = _clearBit(wait_flag, 4)


def _start_timer(watering):
    global timer1, timer2, timer3, timer4, timer5, wait_flag
    if watering == 1:
        timer1 = int_timer
        wait_flag = _setBit(wait_flag, 0)
    elif watering == 2:
        timer2 = int_timer
        wait_flag = _setBit(wait_flag, 1)
    elif watering == 3:
        timer3 = int_timer
        wait_flag = _setBit(wait_flag, 2)
    elif watering == 4:
        timer4 = int_timer
        wait_flag = _setBit(wait_flag, 3)
    elif watering == 5:
        timer5 = int_timer
        wait_flag = _setBit(wait_flag, 4)


def _rain_counter(pin, isSet):
    global bkt1, bkt2, bkt3, bkt4, bkt5, cur_ml1, cur_ml2, cur_ml3, cur_ml4, cur_ml5
    if pin == bkt_pin[1] and isSet:
        bkt1 += 2
        cur_ml1 += 2
        Id = 1
        val = cur_ml1
    elif pin == bkt_pin[2] and isSet:
        bkt2 += 2
        cur_ml2 += 2
        Id = 2
        val = cur_ml2
    elif pin == bkt_pin[3] and isSet:
        bkt3 += 2
        cur_ml3 += 2
        Id = 3
        val = cur_ml3
    elif pin == bkt_pin[4] and isSet:
        bkt4 += 2
        cur_ml4 += 2
        Id = 4
        val = cur_ml4
    elif pin == bkt_pin[5] and isSet:
        bkt5 += 2
        cur_ml5 += 2
        Id = 5
        val = cur_ml5
    if isSet:
        mcastRpc(1, 3, "log", "Tipping", Id, val, "mL")
    print val
    print Id


def _watering_block():
    global Plot_done, watering
    if watering:
        shift = watering - 1
        if not _testBit(Plot_done, shift):
            if not _testBit(wait_flag, shift):
                if _read_current_bkt(watering) >= get_plot_max(watering):  # watering complete for plot
                    Plot_done = _setBit(Plot_done, shift)
                    mcastRpc(1, 3, "log", "Irrgation complete", watering, "", "")
                    _watering_next()
                else:
                    if _read_current_bkt(watering) < get_int_rate(watering):
                        relay_on(watering)
                    else:
                        _update_int_rate(watering)
                        _start_timer(watering)
                        mcastRpc(1, 3, "log", "Irrgation delay", watering, int_timer, "s")
                        _watering_next()
            else:
                _watering_next()

        else:

            if (0b111111 & Plot_done) == 0b111111:
                watering = 0
                relay_off()
                string = "watering complete at all plots"
                mcastRpc(1, 3, "log", "Irrgation complete", "all", "", "")
            else:
                _watering_next()
    return


def start_watering():
    global watering, Plot_done
    watering = 1
    reset_curnt_bkt()
    _update_int_rate(1)
    _update_int_rate(2)
    _update_int_rate(3)
    _update_int_rate(4)
    _update_int_rate(5)
    mcastRpc(1, 3, "log", "Irrgation started", "all", "", "")
    Plot_done = 0b100000


def _watering_next():
    global watering
    if watering < 5:
        watering += 1
    else:
        watering = 1
    relay_off()
    return watering


def get_plot_max(val):
    if val == 1:
        return rate1
    elif val == 2:
        return rate2
    elif val == 3:
        return rate3
    elif val == 4:
        return rate4
    elif val == 5:
        return rate5
    else:
        return 0


def _read_current_bkt(val):
    if val == 1:
        return cur_ml1
    elif val == 2:
        return cur_ml2
    elif val == 3:
        return cur_ml3
    elif val == 4:
        return cur_ml4
    elif val == 5:
        return cur_ml5
    else:
        return 0


def get_int_rate(val):
    if val == 1:
        return chun_ml1
    elif val == 2:
        return chun_ml2
    elif val == 3:
        return chun_ml3
    elif val == 4:
        return chun_ml4
    elif val == 5:
        return chun_ml5
    else:
        return 0


def _update_int_rate(val):
    global chun_ml1, chun_ml2, chun_ml3, chun_ml4, chun_ml5
    if val == 1:
        chun_ml1 += int_rate
    elif val == 2:
        chun_ml2 += int_rate
    elif val == 3:
        chun_ml3 += int_rate
    elif val == 4:
        chun_ml4 += int_rate
    elif val == 5:
        chun_ml5 += int_rate


def plot_maxs(i1, i2, i3, i4, i5):
    global rate1, rate2, rate3, rate4, rate5
    rate1 = i1 * 10
    rate2 = i2 * 10
    rate3 = i3 * 10
    rate4 = i4 * 10
    rate5 = i5 * 10
    saveNvParam(128, rate1)
    saveNvParam(129, rate2)
    saveNvParam(130, rate3)
    saveNvParam(131, rate4)
    saveNvParam(132, rate5)


def reset_curnt_bkt():
    global cur_ml1, cur_ml2, cur_ml3, cur_ml4, cur_ml5, chun_ml1, chun_ml2, chun_ml3, chun_ml4, chun_ml5
    cur_ml1 = 0
    cur_ml2 = 0
    cur_ml3 = 0
    cur_ml4 = 0
    cur_ml5 = 0
    chun_ml1 = 0
    chun_ml2 = 0
    chun_ml3 = 0
    chun_ml4 = 0
    chun_ml5 = 0


def update_int(val1, val2):
    global int_rate, int_timer
    int_rate = int(val1) * 10
    int_timer = int(val2)
    saveNvParam(133, val1)
    saveNvParam(134, val2)

    # testBit() returns a nonzero result, 2**offset, if the bit at 'offset' is one.


def _testBit(int_type, offset):
    mask = 1 << offset
    return (int_type & mask)

    # setBit() returns an integer with the bit at 'offset' set to 1.


def _setBit(int_type, offset):
    mask = 1 << offset
    return (int_type | mask)

    # clearBit() returns an integer with the bit at 'offset' cleared.


def _clearBit(int_type, offset):
    mask = ~(1 << offset)
    return (int_type & mask)
