log_level = 'INFO'
board = '00001'
csv_file = 'irrigation_automation_test.csv'
# snap_serial and snap_type depends on the Operating System
# for linux: '/dev/ttyUSB0'
# for windows com5 could be 4
snap_serial = 4
# SERIAL_TYPE_BTLE = 8
# SERIAL_TYPE_RS232 = 1
# SERIAL_TYPE_SNAPSTICK100 = 2
# SERIAL_TYPE_SNAPSTICK200 = 5
snap_type = 1
