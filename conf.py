import os
import logging

from sys import platform as _platform
import setup


def get_log(log_level):
    if log_level == 'DEBUG':
        return logging.DEBUG
    elif log_level == 'WARNING':
        return logging.WARNING
    elif log_level == 'ERROR':
        return logging.ERROR
    else:
        return logging.INFO


class Configuration:

    def __init__(self):
        self.snapSerial = snap_serial
        self.snapType = snap_type
        self.appPath = appPath
        self.resourcePath = resourcePath
        self.csv_file = csv_file
        self.logFile = logFile
        self.logLevel = logLevel
        self.db = db
        self.board = board


appPath = os.getcwd()
resourcePath = os.path.join(appPath, 'resources')
logFile = os.path.join(resourcePath, 'irrigation.log')
dataPath = os.path.join(appPath, 'automation')
csv_file = os.path.join(dataPath, setup.csv_file) # 'irrigation_automation_test.csv')
db = os.path.join(resourcePath, 'data.db')
logLevel = get_log(setup.log_level) # logging.DEBUG
board = setup.board # '000001'
snap_serial = setup.snap_serial
snap_type = setup.snap_type
