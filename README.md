## Irrigation Automation

This app auto starts sending messages when it opens

Execute
```python
python MAIN.py
```

Edit configuration in
setup.py
```python
log_level = 'INFO'
board = '00001'
csv_file = 'irrigation_automation_test.csv'
```

Once file changed in setup.py go to File > Load New CSV to change automation setup, this file must be stored in the `irrigation` directory

The CSV format file is:

time, func, sol

time HH:MM `24:00`
func `relay_on` or `relay_off`,
sol solenoid `1` or `_` (empty)

```csv
13:00, relay_on, 1
13:30, relay_off,
14:00, relay_on, 1
```

log file (irrigation.log) stored in resources directory

Dependencies
Make sure you only have python 2.x 32 bit installed
```
#python 32 bit !! important
#add python to environment variables
#update pip
#mac or linux
pip install -U pip
#windows
python -m pip install -U pip

pip install TKinter
pip install tornado 
pip install --extra-index-url https://update.synapse-wireless.com/pypi snapconnect
```
